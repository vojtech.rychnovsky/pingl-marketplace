/* eslint-disable no-undef, no-restricted-globals */

// Give the service worker access to Firebase Messaging.
// Note that you can only use Firebase Messaging here, other Firebase libraries
// are not available in the service worker.
importScripts('https://www.gstatic.com/firebasejs/7.14.0/firebase-app.js')
importScripts('https://www.gstatic.com/firebasejs/7.14.0/firebase-messaging.js')

// Initialize the Firebase app in the service worker by passing in the
// messagingSenderId.
if (firebase) {
  var config = {
    apiKey: 'AIzaSyARPQAwTA39I_b508yh1VlJILU9sndNZ5k',
    authDomain: 'pingl-customer.firebaseapp.com',
    databaseURL: 'https://pingl-customer.firebaseio.com',
    projectId: 'pingl-customer',
    storageBucket: 'pingl-customer.appspot.com',
    messagingSenderId: '1074333934101',
    appId: '1:1074333934101:web:6937b7a62356f6d490e41c',
  }
  firebase.initializeApp(config)
  // Retrieve an instance of Firebase Messaging so that it can handle background
  // messages.
  const messaging = firebase.messaging()

  // handle incoming messages in the background
  messaging.setBackgroundMessageHandler((payload) => {
    console.log('Received background message ', payload)

    const type = payload.data.type

    let title = ''
    let body = ''
    if (type === 'TEST') {
      title = 'Pingl - test notification'
      body = 'If you see this message, push notifications are correctly set up.'
    } else if (type === 'ORDER_READY') {
      title = 'Pingl - order is ready!'
      body = 'Your order is ready to pick up!'
    } else if (type === 'NEW_RESTAURANT') {
      title = 'Pingl - new place in your area!'
      body = 'Check this cool places on Pingl'
    } else {
      title = 'Pingl.app'
      body = 'Check Pingl.app for more information'
    }

    const options = {
      body: body,
      icon: '/img/pingl_logo-white.png',
    }

    // fire the notification
    return self.registration.showNotification(title, options)
  })
} else {
  console.error('Firebase could not be loaded. No push notification support')
}

self.addEventListener('notificationclick', function (event) {
  const url = 'https://thesis.web-rychnovsky.com/'
  event.notification.close() // Android needs explicit close.
  event.waitUntil(
    clients.matchAll({ type: 'window' }).then((windowClients) => {
      for (var i = 0; i < windowClients.length; i++) {
        var client = windowClients[i]
        // focus the window with the same url
        if (client.url === url && 'focus' in client) {
          return client.focus()
        }
        // else there is an window, but on different page
        if (client.url.includes(url)) {
          if ('navigate' in client) {
            return client.focus().then((client) => client.navigate(url))
          } else if ('focus' in client) {
            return client.focus()
          }
        }
      }
      // otherwise, there is no window, so create a new one
      if (clients.openWindow) {
        return clients.openWindow(url)
      }
    })
  )
})
