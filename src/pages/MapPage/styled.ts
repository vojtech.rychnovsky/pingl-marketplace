import styled from 'styled-components'

export const MapWrap = styled.div`
  height: 100%;

  & > div {
    height: 100%;
  }
`
