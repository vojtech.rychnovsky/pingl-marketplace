import { advanceTo, clear } from 'jest-date-mock'

import { getRestaurantClosingDate, isCoreDataTimestamp } from '../timeUtils'
import { IOpenTimes } from 'src/data/Restaurant/gql/types'
import { DayOfWeek } from 'src/__generated__/globalTypes'

const mockData: IOpenTimes = [
  {
    day: DayOfWeek.MONDAY,
    time: {
      __typename: 'TimeInterval',
      open: { __typename: 'Time', hour: 8, minute: 0 },
      close: { __typename: 'Time', hour: 18, minute: 0 },
    },
  },
  {
    day: DayOfWeek.TUESDAY,
    time: {
      __typename: 'TimeInterval',
      open: { __typename: 'Time', hour: 8, minute: 0 },
      close: { __typename: 'Time', hour: 18, minute: 0 },
    },
  },
  {
    day: DayOfWeek.WEDNESDAY,
    time: {
      __typename: 'TimeInterval',
      open: { __typename: 'Time', hour: 8, minute: 0 },
      close: { __typename: 'Time', hour: 18, minute: 0 },
    },
  },
  {
    day: DayOfWeek.THURSDAY,
    time: {
      __typename: 'TimeInterval',
      open: { __typename: 'Time', hour: 8, minute: 0 },
      close: { __typename: 'Time', hour: 18, minute: 0 },
    },
  },
  {
    day: DayOfWeek.FRIDAY,
    time: {
      __typename: 'TimeInterval',
      open: { __typename: 'Time', hour: 8, minute: 0 },
      close: { __typename: 'Time', hour: 2, minute: 0 },
    },
  },
  {
    day: DayOfWeek.SATURDAY,
    time: {
      __typename: 'TimeInterval',
      open: { __typename: 'Time', hour: 8, minute: 0 },
      close: { __typename: 'Time', hour: 18, minute: 0 },
    },
  },
  {
    day: DayOfWeek.SUNDAY,
    time: {
      __typename: 'TimeInterval',
      open: { __typename: 'Time', hour: 8, minute: 0 },
      close: { __typename: 'Time', hour: 18, minute: 0 },
    },
  },
]

describe('Time utils', () => {
  afterEach(() => {
    clear()
  })

  it('getRestaurantClosingDate should return correct close time for the same-day closes', async () => {
    // arrange
    advanceTo(new Date('2020-04-30T12:00:00.000')) // reset to Thu Apr 30 2020 12:00:00 GMT+0200
    const openTimes = mockData

    // act
    const closingDate = getRestaurantClosingDate(openTimes)

    // assert
    const expectedDate = new Date()
    expectedDate.setHours(18, 0, 0, 0)

    expect(closingDate.getTime()).toEqual(expectedDate.getTime())
  })

  it('getRestaurantClosingDate should return correct close time for the next-day closes', async () => {
    // arrange
    advanceTo(new Date('2020-05-01T12:00:00.000')) // reset to Fri May 01 2020 12:00:00 GMT+0200
    const openTimes = mockData

    // act
    const closingDate = getRestaurantClosingDate(openTimes)

    // assert
    const expectedDate = new Date()
    expectedDate.setDate(expectedDate.getDate() + 1)
    expectedDate.setHours(2, 0, 0, 0)

    expect(closingDate.getTime()).toEqual(expectedDate.getTime())
  })

  it('isCoreDataTimestamp should recognize core data timestamp and return true', async () => {
    // arrange

    // act
    const result = isCoreDataTimestamp(Date.now() - 978307200000)

    // assert
    expect(result).toEqual(true)
  })

  it('isCoreDataTimestamp should recognize js timestamp and return false', async () => {
    // arrange

    // act
    const result = isCoreDataTimestamp(Date.now())

    // assert
    expect(result).toEqual(false)
  })
})
