import { parseQrCodeUrl, QRCodeParams } from '../qrCodeUtils'

describe('QR code utils', () => {
  it('parseQrCodeUrl should handle table QR code', async () => {
    // arrange
    const url = 'https://example.com?id=xxxx&discount=yyy'

    // act
    const result = parseQrCodeUrl(url)

    // assert
    const expected: QRCodeParams = {
      tableId: 'xxxx',
      discountCode: 'yyy',
      referral: null,
      restaurantSlug: null,
      type: null,
    }
    expect(result).toEqual(expected)
  })

  it('parseQrCodeUrl should handle marketing QR code', async () => {
    // arrange
    const url = 'https://example.com?referral=name&type=takeaway_promo'

    // act
    const result = parseQrCodeUrl(url)

    // assert
    const expected: QRCodeParams = {
      tableId: null,
      discountCode: null,
      referral: 'name',
      restaurantSlug: null,
      type: 'takeaway_promo',
    }
    expect(result).toEqual(expected)
  })

  it('parseQrCodeUrl should handle marketing QR code', async () => {
    // arrange
    const url = 'https://example.com/yummeee'

    // act
    const result = parseQrCodeUrl(url)

    // assert
    const expected: QRCodeParams = {
      tableId: null,
      discountCode: null,
      referral: null,
      restaurantSlug: 'yummeee',
      type: null,
    }
    expect(result).toEqual(expected)
  })
})
