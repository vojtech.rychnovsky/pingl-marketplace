import { generateDeviceUid } from '../notificationsUtils'

describe('Notification utils', () => {
  it('generateDeviceUid should return long enought values', async () => {
    // arrange
    const registeredDevices: string[] = []

    // act
    const deviceId = generateDeviceUid(registeredDevices)

    // assert
    expect(deviceId.length).toBeGreaterThan(20)
  })

  it('generateDeviceUid should return always the same result', async () => {
    // arrange
    const registeredDevices: string[] = []

    // act
    // naively pretend that two results are enought for the test of uniqueness...
    const deviceId = generateDeviceUid(registeredDevices)
    const deviceIdRepeat = generateDeviceUid([...registeredDevices, deviceId])

    // assert
    expect(deviceId).toEqual(deviceIdRepeat)
  })

  it('generateDeviceUid should return new device id if the current is not valid', async () => {
    // arrange
    const registeredDevices: string[] = ['xxx', 'yyy', 'zzz']

    // act
    const deviceId = generateDeviceUid(registeredDevices)
    // do not add the generated id to device list
    const deviceIdRepeat = generateDeviceUid(registeredDevices)

    // assert
    expect(deviceId).not.toEqual(deviceIdRepeat)
  })
})
