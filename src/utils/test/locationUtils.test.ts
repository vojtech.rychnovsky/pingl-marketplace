import { measureDistance, sortRestaurantsByDistance } from '../locationUtils'
import { IRestaurant } from 'src/data/Restaurant/gql/types'

describe('Location utils', () => {
  it('measureDistance should return 0 distance for the same places', async () => {
    // arrange
    const placeA = { lat: 50.002381, lng: 14.44259 }

    // act
    const result = measureDistance(placeA, placeA)

    // assert
    const expected = 0
    expect(result).toEqual(expected)
  })

  it('measureDistance should return correct value with 5% variance from real value', async () => {
    // arrange
    const placeA = { lat: 50.002381, lng: 14.44259 }
    const placeB = { lat: 50.356398, lng: 15.733519 }

    // act
    const result = measureDistance(placeA, placeB)

    // assert
    const expected = 100 * 1000 // in meters
    expect(result).toBeGreaterThan(expected * 0.95)
    expect(result).toBeLessThan(expected * 1.05)
  })

  it('sortRestaurantsByDistance should sort correctly only by distance', async () => {
    // arrange
    const restA = ({
      id: 'xxx',
      name: 'xxx',
      config: { isOpen: true },
      info: { placeLocation: { coordinates: [1, 1] } },
    } as unknown) as IRestaurant

    const restB = ({
      id: 'yyy',
      name: 'yyy',
      config: { isOpen: true },
      info: { placeLocation: { coordinates: [2, 2] } },
    } as unknown) as IRestaurant

    const restC = ({
      id: 'yyy',
      name: 'yyy',
      config: { isOpen: true },
      info: { placeLocation: { coordinates: [3, 3] } },
    } as unknown) as IRestaurant

    const place = { latitude: 0, longitude: 0 }

    // act
    const result = sortRestaurantsByDistance([restC, restB, restA], place)

    // assert
    const expected = [restA, restB, restC]
    expect(result.map(({ distance, ...rest }) => rest)).toEqual(expected)
  })

  it('sortRestaurantsByDistance should place closed places last', async () => {
    // arrange
    const restA = ({
      id: 'xxx',
      name: 'xxx',
      config: { isOpen: true },
      info: { placeLocation: { coordinates: [1, 1] } },
    } as unknown) as IRestaurant

    const restB = ({
      id: 'yyy',
      name: 'yyy',
      config: { isOpen: false },
      info: { placeLocation: { coordinates: [2, 2] } },
    } as unknown) as IRestaurant

    const restC = ({
      id: 'yyy',
      name: 'yyy',
      config: { isOpen: true },
      info: { placeLocation: { coordinates: [3, 3] } },
    } as unknown) as IRestaurant

    const place = { latitude: 0, longitude: 0 }

    // act
    const result = sortRestaurantsByDistance([restB, restA, restC], place)

    // assert
    const expected = [restA, restC, restB]
    expect(result.map(({ distance, ...rest }) => rest)).toEqual(expected)
  })

  it('sortRestaurantsByDistance should place places with missing location last', async () => {
    // arrange
    const restA = ({
      id: 'xxx',
      name: 'xxx',
      config: { isOpen: true },
      info: { placeLocation: { coordinates: [1, 1] } },
    } as unknown) as IRestaurant

    const restB = ({
      id: 'yyy',
      name: 'yyy',
      config: { isOpen: true },
      info: { placeLocation: { coordinates: [] } },
    } as unknown) as IRestaurant

    const restC = ({
      id: 'yyy',
      name: 'yyy',
      config: { isOpen: true },
      info: { placeLocation: { coordinates: [3, 3] } },
    } as unknown) as IRestaurant

    const place = { latitude: 0, longitude: 0 }

    // act
    const result = sortRestaurantsByDistance([restB, restA, restC], place)

    // assert
    const expected = [restA, restC, restB]
    expect(result.map(({ distance, ...rest }) => rest)).toEqual(expected)
  })
})
