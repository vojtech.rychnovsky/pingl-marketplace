// custom IOSNavigator type
type IOSNavigator = Navigator & { standalone: boolean }

export const isStandalone = () => {
  const isInWebAppiOS = (window.navigator as IOSNavigator).standalone === true
  const isInWebAppChrome = window.matchMedia('(display-mode: standalone)')
    .matches
  return isInWebAppiOS || isInWebAppChrome
}
