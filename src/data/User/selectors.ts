import { IReduxState } from '../store'

export const selectUser = (state: IReduxState) => state.user.user

export const selectUserNotificationSettings = (state: IReduxState) =>
  state.user.user?.notifications
