import { action } from 'typesafe-actions'
import { IUser } from './gql/types'

import { LOAD_USER_SUCCESS } from './constants'

export const loadUserSuccess = (user: IUser) => action(LOAD_USER_SUCCESS, user)
