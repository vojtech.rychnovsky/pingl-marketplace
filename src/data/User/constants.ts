// user loading
export const LOAD_USER_SUCCESS = '@user/LOAD_USER_SUCCESS'

export default {
  LOAD_USER_SUCCESS,
}
