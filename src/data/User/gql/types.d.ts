import {
  PersonFragment,
  PersonFragment_creditCards,
} from './__generated__/PersonFragment'

export type IUser = ImmutableObject<Omit<PersonFragment, '__typename'>>

export type ICreditCard = ImmutableObject<PersonFragment_creditCards>
