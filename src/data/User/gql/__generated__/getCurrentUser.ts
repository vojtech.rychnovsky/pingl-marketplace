/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { LinkedSocial, Role } from "./../../../../__generated__/globalTypes";

// ====================================================
// GraphQL query operation: getCurrentUser
// ====================================================

export interface getCurrentUser_getCurrentUser_notifications {
  __typename: "NotificationsConfig";
  fcmDevices: string[];
  isMessengerEnabled: boolean;
  isEmailEnabled: boolean;
}

export interface getCurrentUser_getCurrentUser_creditCards {
  __typename: "CreditCard";
  id: string;
  longMaskedCln: string;
}

export interface getCurrentUser_getCurrentUser {
  __typename: "Person";
  id: string;
  email: string;
  /**
   * pass: String
   */
  name: string | null;
  givenName: string | null;
  familyName: string | null;
  pictureUrl: string | null;
  linkedSocial: LinkedSocial;
  roles: Role[];
  notifications: getCurrentUser_getCurrentUser_notifications;
  creditCards: getCurrentUser_getCurrentUser_creditCards[];
  defaultCreditCard: string;
}

export interface getCurrentUser {
  getCurrentUser: getCurrentUser_getCurrentUser;
}
