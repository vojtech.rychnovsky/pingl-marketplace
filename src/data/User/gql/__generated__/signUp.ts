/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { LinkedSocial, Role } from "./../../../../__generated__/globalTypes";

// ====================================================
// GraphQL mutation operation: signUp
// ====================================================

export interface signUp_signUp_user_notifications {
  __typename: "NotificationsConfig";
  fcmDevices: string[];
  isMessengerEnabled: boolean;
  isEmailEnabled: boolean;
}

export interface signUp_signUp_user_creditCards {
  __typename: "CreditCard";
  id: string;
  longMaskedCln: string;
}

export interface signUp_signUp_user {
  __typename: "Person";
  id: string;
  email: string;
  /**
   * pass: String
   */
  name: string | null;
  givenName: string | null;
  familyName: string | null;
  pictureUrl: string | null;
  linkedSocial: LinkedSocial;
  roles: Role[];
  notifications: signUp_signUp_user_notifications;
  creditCards: signUp_signUp_user_creditCards[];
  defaultCreditCard: string;
}

export interface signUp_signUp {
  __typename: "LoginResponse";
  user: signUp_signUp_user;
  token: string;
}

export interface signUp {
  signUp: signUp_signUp;
}

export interface signUpVariables {
  username: string;
  password: string;
  givenName: string;
  familyName: string;
}
