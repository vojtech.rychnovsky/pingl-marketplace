/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: setPersonNotificationsIsEmailEnabled
// ====================================================

export interface setPersonNotificationsIsEmailEnabled {
  setPersonNotificationsIsEmailEnabled: boolean;
}

export interface setPersonNotificationsIsEmailEnabledVariables {
  isEnabled: boolean;
}
