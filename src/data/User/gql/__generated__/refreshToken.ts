/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: refreshToken
// ====================================================

export interface refreshToken {
  refreshToken: string;
}
