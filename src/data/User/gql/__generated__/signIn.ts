/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { LinkedSocial, Role } from "./../../../../__generated__/globalTypes";

// ====================================================
// GraphQL mutation operation: signIn
// ====================================================

export interface signIn_signIn_user_notifications {
  __typename: "NotificationsConfig";
  fcmDevices: string[];
  isMessengerEnabled: boolean;
  isEmailEnabled: boolean;
}

export interface signIn_signIn_user_creditCards {
  __typename: "CreditCard";
  id: string;
  longMaskedCln: string;
}

export interface signIn_signIn_user {
  __typename: "Person";
  id: string;
  email: string;
  /**
   * pass: String
   */
  name: string | null;
  givenName: string | null;
  familyName: string | null;
  pictureUrl: string | null;
  linkedSocial: LinkedSocial;
  roles: Role[];
  notifications: signIn_signIn_user_notifications;
  creditCards: signIn_signIn_user_creditCards[];
  defaultCreditCard: string;
}

export interface signIn_signIn {
  __typename: "LoginResponse";
  user: signIn_signIn_user;
  token: string;
}

export interface signIn {
  /**
   * PERSON
   */
  signIn: signIn_signIn;
}

export interface signInVariables {
  username: string;
  password?: string | null;
}
