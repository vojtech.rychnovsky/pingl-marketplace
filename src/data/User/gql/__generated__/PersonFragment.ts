/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { LinkedSocial, Role } from "./../../../../__generated__/globalTypes";

// ====================================================
// GraphQL fragment: PersonFragment
// ====================================================

export interface PersonFragment_notifications {
  __typename: "NotificationsConfig";
  fcmDevices: string[];
  isMessengerEnabled: boolean;
  isEmailEnabled: boolean;
}

export interface PersonFragment_creditCards {
  __typename: "CreditCard";
  id: string;
  longMaskedCln: string;
}

export interface PersonFragment {
  __typename: "Person";
  id: string;
  email: string;
  /**
   * pass: String
   */
  name: string | null;
  givenName: string | null;
  familyName: string | null;
  pictureUrl: string | null;
  linkedSocial: LinkedSocial;
  roles: Role[];
  notifications: PersonFragment_notifications;
  creditCards: PersonFragment_creditCards[];
  defaultCreditCard: string;
}
