import reducer, { IUserState } from '../reducers'
import { loadUserSuccess } from '../actions'
import { IUser } from '../gql/types'
import { resetLogin } from 'src/data/globalActions'

describe('User reducer', () => {
  it('should return the initial state', () => {
    // arrange
    const initState = undefined
    const action = {} as any

    // act
    const state = reducer(initState, action)

    // assert
    const expected: IUserState = { user: null }
    expect(state).toEqual(expected)
  })

  it('should save the user to the store', () => {
    // arrange
    const initState = undefined
    const action = {} as any
    const state = reducer(initState, action)
    const user: IUser = {
      id: 'xx',
      name: 'Test user',
    } as IUser

    // act
    const result = reducer(state, loadUserSuccess(user) as any)

    // assert
    const expected: IUserState = { user }
    expect(result).toEqual(expected)
  })

  it('should be deleted after logout', () => {
    // arrange
    const user: IUser = {
      id: 'xx',
      name: 'Test user',
    } as IUser

    // act
    const result = reducer({ user }, resetLogin() as any)

    // assert
    const expected: IUserState = { user: null }
    expect(result).toEqual(expected)
  })
})
