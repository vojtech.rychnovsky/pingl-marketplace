import { action, ActionType } from 'typesafe-actions'
import { GLOBAL_RESET_STORE } from './globalConstants'

// to be implemented by specific reducers

// fires when user wants to logout
export const resetLogin = () => action(GLOBAL_RESET_STORE)

export type IGlobalActions = ActionType<typeof resetLogin>
