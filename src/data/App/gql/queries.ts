import gql from 'graphql-tag'

export const SEND_TEST_FCM_NOTIFICATION = gql`
  query sendTestFCMNotification {
    sendTestFCMNotification
  }
`
