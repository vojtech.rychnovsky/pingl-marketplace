import { IAppState } from '../reducers'
import { LocationProvider } from '../gql/types'
import { selectCurrentLocation } from '../selectors'
import { IReduxState } from 'src/data/store'

describe('App selectors', () => {
  it('should select the current location', () => {
    // arrange
    const coords = { accuracy: 0, latitude: 1, longitude: 2 } as Coordinates
    const provider: LocationProvider = LocationProvider.NAVIGATOR
    const timestamp = Date.now()

    const appState: IAppState = {
      currentLocation: {
        coords,
        provider,
        timestamp,
      },
    }
    const state = { app: appState } as IReduxState

    // act
    const result = selectCurrentLocation(state)

    // assert
    expect(result).not.toEqual(null)
  })

  it('should return null because the location is week old', () => {
    // arrange
    const coords = { accuracy: 0, latitude: 1, longitude: 2 } as Coordinates
    const provider: LocationProvider = LocationProvider.NAVIGATOR
    const date = new Date()
    date.setDate(date.getDate() - 7)
    const timestamp = date.getTime()

    const appState: IAppState = {
      currentLocation: {
        coords,
        provider,
        timestamp,
      },
    }
    const state = { app: appState } as IReduxState

    // act
    const result = selectCurrentLocation(state)

    // assert
    expect(result).toEqual(null)
  })
})
