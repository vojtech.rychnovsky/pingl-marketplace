import reducer, { IAppState } from '../reducers'
import { saveCurrentLocation } from '../actions'
import { LocationProvider } from '../gql/types'

describe('App reducer', () => {
  it('should return the initial state', () => {
    // arrange
    const initState = undefined
    const action = {} as any

    // act
    const state = reducer(initState, action)

    // assert
    const expected: IAppState = {
      currentLocation: undefined,
    }
    expect(state).toEqual(expected)
  })

  it('should save the current location to the store', () => {
    // arrange
    const initState = undefined
    const action = {} as any
    const state = reducer(initState, action)

    const coords = { accuracy: 0, latitude: 1, longitude: 2 } as Coordinates
    const provider: LocationProvider = LocationProvider.NAVIGATOR
    const timestamp = Date.now()

    // act
    const result = reducer(
      state,
      saveCurrentLocation({ coords, timestamp, provider })
    )

    // assert
    const expected: IAppState = {
      ...state,
      currentLocation: {
        provider: provider,
        timestamp,
        coords: coords,
      },
    }
    expect(result).toEqual(expected)
  })

  it('should save the current location with core data timestamp (Safari) to the store', () => {
    // arrange
    const initState = undefined
    const action = {} as any
    const state = reducer(initState, action)

    const coords = { accuracy: 0, latitude: 1, longitude: 2 } as Coordinates
    const provider: LocationProvider = LocationProvider.NAVIGATOR
    const timestamp = Date.now()

    // act
    const result = reducer(
      state,
      saveCurrentLocation({
        coords,
        timestamp: timestamp - 978307200000,
        provider,
      })
    )

    // assert
    const expected: IAppState = {
      ...state,
      currentLocation: {
        provider: provider,
        timestamp,
        coords: coords,
      },
    }
    expect(result).toEqual(expected)
  })
})
