import { action } from 'typesafe-actions'
import { SAVE_CURRENT_LOCATION } from './constants'
import { LocationProvider } from './gql/types'

export const saveCurrentLocation = (args: {
  coords: Coordinates
  provider: LocationProvider
  timestamp?: number
}) => action(SAVE_CURRENT_LOCATION, args)
