import { MenuItemFragment } from './__generated__/MenuItemFragment'

export type IMenuItem = ImmutableObject<Omit<MenuItemFragment, '__typename'>>

type IRestaurantMenuItem = IMenuItem & { restaurantId: string }
