/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: MenuItemFragment
// ====================================================

export interface MenuItemFragment_options_values {
  __typename: "OptionValue";
  id: string;
  name: string;
  price: number;
  discountedPrice: number | null;
}

export interface MenuItemFragment_options {
  __typename: "Option";
  id: string;
  name: string;
  values: MenuItemFragment_options_values[];
}

export interface MenuItemFragment {
  __typename: "MenuItem";
  id: string;
  name: string;
  imageUrl: string | null;
  description: string;
  isActive: boolean;
  options: MenuItemFragment_options[];
}
