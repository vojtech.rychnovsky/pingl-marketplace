/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OrderType } from "./../../../../__generated__/globalTypes";

// ====================================================
// GraphQL query operation: getSpecialOfferMenuItems
// ====================================================

export interface getSpecialOfferMenuItems_getSpecialOfferMenuItems_items_options_values {
  __typename: "OptionValue";
  id: string;
  name: string;
  price: number;
  discountedPrice: number | null;
}

export interface getSpecialOfferMenuItems_getSpecialOfferMenuItems_items_options {
  __typename: "Option";
  id: string;
  name: string;
  values: getSpecialOfferMenuItems_getSpecialOfferMenuItems_items_options_values[];
}

export interface getSpecialOfferMenuItems_getSpecialOfferMenuItems_items {
  __typename: "MenuItem";
  id: string;
  name: string;
  imageUrl: string | null;
  description: string;
  isActive: boolean;
  options: getSpecialOfferMenuItems_getSpecialOfferMenuItems_items_options[];
}

export interface getSpecialOfferMenuItems_getSpecialOfferMenuItems_discount {
  __typename: "DiscountPublic";
  id: string;
  name: string;
  code: string;
}

export interface getSpecialOfferMenuItems_getSpecialOfferMenuItems {
  __typename: "GetRecommendedItemsResponse";
  items: getSpecialOfferMenuItems_getSpecialOfferMenuItems_items[];
  discount: getSpecialOfferMenuItems_getSpecialOfferMenuItems_discount | null;
}

export interface getSpecialOfferMenuItems {
  getSpecialOfferMenuItems: getSpecialOfferMenuItems_getSpecialOfferMenuItems;
}

export interface getSpecialOfferMenuItemsVariables {
  restaurantId: string;
  orderType: OrderType;
  discountId?: string | null;
}
