import reducer, { IMenuItemsState } from '../reducers'
import {
  loadSpecialOfferSuccess,
  deleteOtherSpecialOffersThanFromTheseRestaurants,
  saveSpecialOffer,
} from '../actions'
import { IMenuItem } from '../gql/types'

const mockMenuItems = (): IMenuItem[] => {
  return [
    ({
      id: 'aaa',
      name: 'tets',
      description: 'test',
      isActive: true,
    } as unknown) as IMenuItem,
    ({
      id: 'bbb',
      name: 'test',
      description: 'test',
      isActive: true,
    } as unknown) as IMenuItem,
  ]
}

describe('Menu items reducer', () => {
  it('should return the initial state', () => {
    // arrange
    const initState = undefined
    const action = {} as any

    // act
    const state = reducer(initState, action)

    // assert
    const expected: IMenuItemsState = {}
    expect(state).toEqual(expected)
  })

  it('should save the menuItems to the store', () => {
    // arrange
    const initState = {}
    const restaurantId = 'xxx'
    const menuItems = mockMenuItems()

    // act
    const result = reducer(
      initState,
      saveSpecialOffer({ restaurantId, menuItems })
    )

    // assert
    const expected: IMenuItemsState = {
      [restaurantId]: { specialOffer: menuItems },
    }
    expect(result).toEqual(expected)
  })

  it('should save multiple restaurants menuItems to the store', () => {
    // arrange
    const initState = { a: { specialOffer: [] } }
    const restaurantIdA = 'xxx'
    const restaurantIdB = 'yyy'
    const menuItems = mockMenuItems()

    // act
    const result = reducer(
      initState,
      loadSpecialOfferSuccess({
        [restaurantIdA]: { specialOffer: menuItems },
        [restaurantIdB]: { specialOffer: menuItems },
      })
    )

    // assert
    const expected: IMenuItemsState = {
      [restaurantIdA]: { specialOffer: menuItems },
      [restaurantIdB]: { specialOffer: menuItems },
    }
    expect(result).toEqual(expected)
  })

  it('should delete removed restaurants from the store', () => {
    // arrange
    const restaurantIdA = 'xxx'
    const restaurantIdB = 'yyy'
    const menuItems = mockMenuItems()
    const initState: IMenuItemsState = {
      [restaurantIdA]: { specialOffer: menuItems },
      [restaurantIdB]: { specialOffer: menuItems },
    }

    // act
    const result = reducer(
      initState,
      deleteOtherSpecialOffersThanFromTheseRestaurants({
        restaurantIds: [restaurantIdA],
      })
    )

    // assert
    const expected: IMenuItemsState = {
      [restaurantIdA]: { specialOffer: menuItems },
    }
    expect(result).toEqual(expected)
  })
})
