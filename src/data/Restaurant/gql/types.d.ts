import {
  RestaurantFragment,
  RestaurantFragment_config_openTimes,
} from './__generated__/RestaurantFragment'
import { RestaurantFilter } from 'src/__generated__/globalTypes'
import { DiscountFragment } from './__generated__/DiscountFragment'
import { TagFragment } from './__generated__/TagFragment'

export type IRestaurant = ImmutableObject<
  Omit<RestaurantFragment, '__typename'>
>

export type IRestaurantWithDistance = IRestaurant & { distance?: number }

export type IOpenTimes = ImmutableArray<
  Omit<RestaurantFragment_config_openTimes, '__typename'>
>

export type IRestaurantsFilter = RestaurantFilter

export type IDiscount = ImmutableObject<Omit<DiscountFragment, '__typename'>>

export type ITag = ImmutableObject<Omit<TagFragment, '__typename'>>
