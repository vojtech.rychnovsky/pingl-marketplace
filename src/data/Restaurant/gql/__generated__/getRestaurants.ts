/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { RestaurantFilter, OrderType, DayOfWeek } from "./../../../../__generated__/globalTypes";

// ====================================================
// GraphQL query operation: getRestaurants
// ====================================================

export interface getRestaurants_getRestaurants_info_placeLocation {
  __typename: "Location";
  coordinates: number[];
}

export interface getRestaurants_getRestaurants_info_tags {
  __typename: "Tag";
  id: string;
  name: string;
}

export interface getRestaurants_getRestaurants_info {
  __typename: "RestaurantInfo";
  phone: string;
  website: string | null;
  websiteUrl: string | null;
  instagramUrl: string | null;
  facebookUrl: string | null;
  coverPhotoUrl: string;
  placeAddress: string;
  placeLocation: getRestaurants_getRestaurants_info_placeLocation;
  tags: getRestaurants_getRestaurants_info_tags[];
}

export interface getRestaurants_getRestaurants_tables {
  __typename: "Table";
  id: string;
  name: string;
  orderType: OrderType;
}

export interface getRestaurants_getRestaurants_config_languages {
  __typename: "Language";
  locale: string;
}

export interface getRestaurants_getRestaurants_config_customer {
  __typename: "CustomerConfig";
  tipsEnabled: boolean;
  urlFriendlyName: string;
}

export interface getRestaurants_getRestaurants_config_openTimes_time_open {
  __typename: "Time";
  hour: number;
  minute: number;
}

export interface getRestaurants_getRestaurants_config_openTimes_time_close {
  __typename: "Time";
  hour: number;
  minute: number;
}

export interface getRestaurants_getRestaurants_config_openTimes_time {
  __typename: "TimeInterval";
  open: getRestaurants_getRestaurants_config_openTimes_time_open;
  close: getRestaurants_getRestaurants_config_openTimes_time_close;
}

export interface getRestaurants_getRestaurants_config_openTimes {
  __typename: "ActiveTime";
  day: DayOfWeek;
  time: getRestaurants_getRestaurants_config_openTimes_time;
}

export interface getRestaurants_getRestaurants_config {
  __typename: "RestaurantConfig";
  languages: getRestaurants_getRestaurants_config_languages[];
  customer: getRestaurants_getRestaurants_config_customer;
  openTimes: getRestaurants_getRestaurants_config_openTimes[];
  isOpen: boolean;
  isActive: boolean;
  orderTypes: OrderType[];
}

export interface getRestaurants_getRestaurants {
  __typename: "RestaurantPublic";
  id: string;
  name: string;
  info: getRestaurants_getRestaurants_info;
  tables: getRestaurants_getRestaurants_tables[];
  config: getRestaurants_getRestaurants_config;
}

export interface getRestaurants {
  getRestaurants: getRestaurants_getRestaurants[];
}

export interface getRestaurantsVariables {
  filter?: RestaurantFilter | null;
}
