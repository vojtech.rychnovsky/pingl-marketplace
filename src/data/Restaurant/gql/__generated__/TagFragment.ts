/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: TagFragment
// ====================================================

export interface TagFragment {
  __typename: "Tag";
  id: string;
  name: string;
}
