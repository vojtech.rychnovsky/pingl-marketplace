/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: getAllTags
// ====================================================

export interface getAllTags_getAllTags {
  __typename: "Tag";
  id: string;
  name: string;
}

export interface getAllTags {
  getAllTags: getAllTags_getAllTags[];
}
