/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OrderType, DayOfWeek } from "./../../../../__generated__/globalTypes";

// ====================================================
// GraphQL query operation: getRestaurantByUrlFriendlyName
// ====================================================

export interface getRestaurantByUrlFriendlyName_getRestaurantByUrlFriendlyName_info_placeLocation {
  __typename: "Location";
  coordinates: number[];
}

export interface getRestaurantByUrlFriendlyName_getRestaurantByUrlFriendlyName_info_tags {
  __typename: "Tag";
  id: string;
  name: string;
}

export interface getRestaurantByUrlFriendlyName_getRestaurantByUrlFriendlyName_info {
  __typename: "RestaurantInfo";
  phone: string;
  website: string | null;
  websiteUrl: string | null;
  instagramUrl: string | null;
  facebookUrl: string | null;
  coverPhotoUrl: string;
  placeAddress: string;
  placeLocation: getRestaurantByUrlFriendlyName_getRestaurantByUrlFriendlyName_info_placeLocation;
  tags: getRestaurantByUrlFriendlyName_getRestaurantByUrlFriendlyName_info_tags[];
}

export interface getRestaurantByUrlFriendlyName_getRestaurantByUrlFriendlyName_tables {
  __typename: "Table";
  id: string;
  name: string;
  orderType: OrderType;
}

export interface getRestaurantByUrlFriendlyName_getRestaurantByUrlFriendlyName_config_languages {
  __typename: "Language";
  locale: string;
}

export interface getRestaurantByUrlFriendlyName_getRestaurantByUrlFriendlyName_config_customer {
  __typename: "CustomerConfig";
  tipsEnabled: boolean;
  urlFriendlyName: string;
}

export interface getRestaurantByUrlFriendlyName_getRestaurantByUrlFriendlyName_config_openTimes_time_open {
  __typename: "Time";
  hour: number;
  minute: number;
}

export interface getRestaurantByUrlFriendlyName_getRestaurantByUrlFriendlyName_config_openTimes_time_close {
  __typename: "Time";
  hour: number;
  minute: number;
}

export interface getRestaurantByUrlFriendlyName_getRestaurantByUrlFriendlyName_config_openTimes_time {
  __typename: "TimeInterval";
  open: getRestaurantByUrlFriendlyName_getRestaurantByUrlFriendlyName_config_openTimes_time_open;
  close: getRestaurantByUrlFriendlyName_getRestaurantByUrlFriendlyName_config_openTimes_time_close;
}

export interface getRestaurantByUrlFriendlyName_getRestaurantByUrlFriendlyName_config_openTimes {
  __typename: "ActiveTime";
  day: DayOfWeek;
  time: getRestaurantByUrlFriendlyName_getRestaurantByUrlFriendlyName_config_openTimes_time;
}

export interface getRestaurantByUrlFriendlyName_getRestaurantByUrlFriendlyName_config {
  __typename: "RestaurantConfig";
  languages: getRestaurantByUrlFriendlyName_getRestaurantByUrlFriendlyName_config_languages[];
  customer: getRestaurantByUrlFriendlyName_getRestaurantByUrlFriendlyName_config_customer;
  openTimes: getRestaurantByUrlFriendlyName_getRestaurantByUrlFriendlyName_config_openTimes[];
  isOpen: boolean;
  isActive: boolean;
  orderTypes: OrderType[];
}

export interface getRestaurantByUrlFriendlyName_getRestaurantByUrlFriendlyName {
  __typename: "RestaurantPublic";
  id: string;
  name: string;
  info: getRestaurantByUrlFriendlyName_getRestaurantByUrlFriendlyName_info;
  tables: getRestaurantByUrlFriendlyName_getRestaurantByUrlFriendlyName_tables[];
  config: getRestaurantByUrlFriendlyName_getRestaurantByUrlFriendlyName_config;
}

export interface getRestaurantByUrlFriendlyName {
  getRestaurantByUrlFriendlyName: getRestaurantByUrlFriendlyName_getRestaurantByUrlFriendlyName;
}

export interface getRestaurantByUrlFriendlyNameVariables {
  name: string;
}
