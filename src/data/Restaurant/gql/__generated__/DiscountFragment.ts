/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: DiscountFragment
// ====================================================

export interface DiscountFragment {
  __typename: "DiscountPublic";
  id: string;
  name: string;
  code: string;
}
