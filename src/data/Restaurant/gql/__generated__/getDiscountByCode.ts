/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: getDiscountByCode
// ====================================================

export interface getDiscountByCode_getDiscountByCode {
  __typename: "DiscountPublic";
  id: string;
  name: string;
  code: string;
}

export interface getDiscountByCode {
  getDiscountByCode: getDiscountByCode_getDiscountByCode;
}

export interface getDiscountByCodeVariables {
  code: string;
  restaurantId: string;
}
