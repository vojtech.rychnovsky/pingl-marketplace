/* tslint:disable */
/* eslint-disable */
// @generated
// This file was automatically generated and should not be edited.

import { OrderType, DayOfWeek } from "./../../../../__generated__/globalTypes";

// ====================================================
// GraphQL query operation: getRestaurantByTableId
// ====================================================

export interface getRestaurantByTableId_getRestaurantByTableId_info_placeLocation {
  __typename: "Location";
  coordinates: number[];
}

export interface getRestaurantByTableId_getRestaurantByTableId_info_tags {
  __typename: "Tag";
  id: string;
  name: string;
}

export interface getRestaurantByTableId_getRestaurantByTableId_info {
  __typename: "RestaurantInfo";
  phone: string;
  website: string | null;
  websiteUrl: string | null;
  instagramUrl: string | null;
  facebookUrl: string | null;
  coverPhotoUrl: string;
  placeAddress: string;
  placeLocation: getRestaurantByTableId_getRestaurantByTableId_info_placeLocation;
  tags: getRestaurantByTableId_getRestaurantByTableId_info_tags[];
}

export interface getRestaurantByTableId_getRestaurantByTableId_tables {
  __typename: "Table";
  id: string;
  name: string;
  orderType: OrderType;
}

export interface getRestaurantByTableId_getRestaurantByTableId_config_languages {
  __typename: "Language";
  locale: string;
}

export interface getRestaurantByTableId_getRestaurantByTableId_config_customer {
  __typename: "CustomerConfig";
  tipsEnabled: boolean;
  urlFriendlyName: string;
}

export interface getRestaurantByTableId_getRestaurantByTableId_config_openTimes_time_open {
  __typename: "Time";
  hour: number;
  minute: number;
}

export interface getRestaurantByTableId_getRestaurantByTableId_config_openTimes_time_close {
  __typename: "Time";
  hour: number;
  minute: number;
}

export interface getRestaurantByTableId_getRestaurantByTableId_config_openTimes_time {
  __typename: "TimeInterval";
  open: getRestaurantByTableId_getRestaurantByTableId_config_openTimes_time_open;
  close: getRestaurantByTableId_getRestaurantByTableId_config_openTimes_time_close;
}

export interface getRestaurantByTableId_getRestaurantByTableId_config_openTimes {
  __typename: "ActiveTime";
  day: DayOfWeek;
  time: getRestaurantByTableId_getRestaurantByTableId_config_openTimes_time;
}

export interface getRestaurantByTableId_getRestaurantByTableId_config {
  __typename: "RestaurantConfig";
  languages: getRestaurantByTableId_getRestaurantByTableId_config_languages[];
  customer: getRestaurantByTableId_getRestaurantByTableId_config_customer;
  openTimes: getRestaurantByTableId_getRestaurantByTableId_config_openTimes[];
  isOpen: boolean;
  isActive: boolean;
  orderTypes: OrderType[];
}

export interface getRestaurantByTableId_getRestaurantByTableId {
  __typename: "RestaurantPublic";
  id: string;
  name: string;
  info: getRestaurantByTableId_getRestaurantByTableId_info;
  tables: getRestaurantByTableId_getRestaurantByTableId_tables[];
  config: getRestaurantByTableId_getRestaurantByTableId_config;
}

export interface getRestaurantByTableId {
  getRestaurantByTableId: getRestaurantByTableId_getRestaurantByTableId;
}

export interface getRestaurantByTableIdVariables {
  tableId: string;
}
