import { handleGetRestaurants } from '../thunks'
import {
  loadRestaurantsSuccess,
  loadRestaurantsRequest,
  loadRestaurantsFailure,
} from '../actions'
import { getRestaurants_getRestaurants } from '../gql/__generated__/getRestaurants'
import * as restaurantApi from '../api'

// mock each api call
jest.mock('../api', () => ({
  requestGetRestaurants: jest.fn(),
}))

// mockedTypes
const mockedRequestGetRestaurants = (restaurantApi.requestGetRestaurants as unknown) as jest.Mock<
  ReturnType<typeof restaurantApi.requestGetRestaurants>
>

// thunk mock
const mockStore = () => ({
  getState: jest.fn(),
  dispatch: jest.fn(),
})

describe('Get Restaurants Thunk', () => {
  it('should call dispatch with request and success actions', async () => {
    // arrange
    const restaurants: getRestaurants_getRestaurants[] = []
    mockedRequestGetRestaurants.mockReturnValue(
      new Promise((resolve) => resolve([]))
    )
    const { getState, dispatch } = mockStore()

    // act
    await handleGetRestaurants()(dispatch, getState, undefined)

    // assert
    expect(dispatch).toHaveBeenNthCalledWith(1, loadRestaurantsRequest())
    expect(dispatch).toHaveBeenNthCalledWith(
      2,
      loadRestaurantsSuccess(restaurants)
    )
  })

  it('should call dispatch with request and failure actions if the api requests failes', async () => {
    // arrange
    mockedRequestGetRestaurants.mockRejectedValue(new Error('Custom error'))
    const { getState, dispatch } = mockStore()

    // act
    await handleGetRestaurants()(dispatch, getState, undefined)

    // assert
    expect(dispatch).toHaveBeenNthCalledWith(1, loadRestaurantsRequest())
    expect(dispatch).toHaveBeenNthCalledWith(2, loadRestaurantsFailure())
  })
})
