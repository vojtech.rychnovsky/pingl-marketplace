import reducer, { IRestaurantState } from '../reducers'
import {
  loadRestaurantsSuccess,
  saveSelectedRestaurantId,
  saveSelectedTableId,
  saveSelectedDiscount,
  loadTagsSuccess,
} from '../actions'
import { IRestaurant, IDiscount, ITag } from '../gql/types'

describe('Restaurant reducer', () => {
  it('should return the initial state', () => {
    // arrange
    const initState = undefined
    const action = {} as any

    // act
    const state = reducer(initState, action)

    // assert
    const expected: IRestaurantState = {
      restaurants: [],
      selectedId: undefined,
      selectedTableId: undefined,
      selectedMenuItemId: undefined,
      selectedDiscount: undefined,
      tags: [],
    }
    expect(state).toEqual(expected)
  })

  it('should save the restaurants to the store', () => {
    // arrange
    const initState = undefined
    const action = {} as any
    const state = reducer(initState, action)
    const restaurants: IRestaurant[] = [
      {
        id: 'xx',
        name: 'Test restaurant',
      } as IRestaurant,
    ]

    // act
    const result = reducer(state, loadRestaurantsSuccess(restaurants))

    // assert
    const expected: IRestaurantState = { ...state, restaurants }
    expect(result).toEqual(expected)
  })

  it('should save selectedId to the store', () => {
    // arrange
    const initState = undefined
    const action = {} as any
    const state = reducer(initState, action)
    const restaurantId: string = 'xxx'

    // act
    const result = reducer(state, saveSelectedRestaurantId(restaurantId))

    // assert
    const expected: IRestaurantState = { ...state, selectedId: restaurantId }
    expect(result).toEqual(expected)
  })

  it('should save selectedTableId to the store', () => {
    // arrange
    const initState = undefined
    const action = {} as any
    const state = reducer(initState, action)
    const tableId: string = 'xxx'

    // act
    const result = reducer(state, saveSelectedTableId(tableId))

    // assert
    const expected: IRestaurantState = { ...state, selectedTableId: tableId }
    expect(result).toEqual(expected)
  })

  it('should save discount to the store', () => {
    // arrange
    const initState = undefined
    const action = {} as any
    const state = reducer(initState, action)
    const discount: IDiscount = { id: 'id', code: 'code', name: 'name' }

    // act
    const result = reducer(state, saveSelectedDiscount(discount))

    // assert
    const expected: IRestaurantState = { ...state, selectedDiscount: discount }
    expect(result).toEqual(expected)
  })

  it('should save tags to the store', () => {
    // arrange
    const initState = undefined
    const action = {} as any
    const state = reducer(initState, action)
    const tags: ITag[] = [
      {
        id: 'xxx',
        name: 'xxxx',
      },
    ]

    // act
    const result = reducer(state, loadTagsSuccess(tags))

    // assert
    const expected: IRestaurantState = { ...state, tags }
    expect(result).toEqual(expected)
  })
})
