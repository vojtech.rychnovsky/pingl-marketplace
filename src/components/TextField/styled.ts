import styled from 'styled-components'
import MaterialInputAdornment from '@material-ui/core/InputAdornment'

export const InputAdornment = styled(MaterialInputAdornment)`
  min-width: 40px;
  align-items: center;
  justify-content: center;
`
