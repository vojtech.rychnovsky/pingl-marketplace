import styled from 'styled-components'
import { colors, radius } from 'src/styles'

export const Wrap = styled.div`
  color: ${colors.error};
  border-radius: ${radius.secondary};
  padding: 16px;
  background: ${colors.error_light};
`
