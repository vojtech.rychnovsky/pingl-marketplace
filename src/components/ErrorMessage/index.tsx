import React from 'react'

import * as S from './styled'

const ErrorMessage: React.FC = ({ children, ...rest }) => {
  return <S.Wrap {...rest}>{children}</S.Wrap>
}

export default ErrorMessage
