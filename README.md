# Ping.app prototype

This project is a prototype of Pingl.app PWA created as a bachelor project at the Czech Technical University.

This project was bootstrapped with [Create React App (CRA)](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.<br />
Currently there ar no tests.

### `npm run build:local`

Builds the app for production to the `build` folder.<br />
It correctly bundles React in production mode and optimizes the build for the best performance.

### `npm run build:sw`

Transpiles Typescript service worker to JavaScript service worker to the `build` folder. Internally, it uses `Babel` and `workbox.injectManifest`. It should be run after the app is built. Requires globally installed `node`.

### `npm run clean-default-cra-sw`

Helper function. Removes the default service worker and precache manifest created by CRA.

### `npm run format, npm run lint:ts, npm run lint:css`

Formats and lints the code according to `prettier` / `eslint` / `stylelint` configuration.

### `npm run schema:fetch`

Fetches the up to date GraphQL schema from remote dev server.

### `npm run schema:generate`

Generates types for all `gql` queries, mutations and subscriptions. Generated types are localed in `src/__generated__/` folder and in the same folder as every `gql` query.<br/>
Schema should be fetched first by `npm run schema:fetch` script.

### `npm run splash:generate`

Generates iOS splash screens for both classic and dark mode. <br/>
It uses the source files located in `/public/pwa-assets/splash/` folder. For dark theme, `dark-source.svg` is used, `light-source.svg` is used for classic theme. <br/>
Android splash screen is configured separatly in Web App Manifest.

## Project Documentation

This project is documented by semestral project report.

You can learn more about the CRA project in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

## Author

Vojtěch Rychnovský
Faculty of Electrical Engineering, Czech Technical Univerzity in Prague
