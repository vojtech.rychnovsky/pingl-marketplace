const workboxBuild = require('workbox-build')
const babel = require('@babel/core')
const fs = require('fs')

/**
 * Service Worker Compiler from Typescript to Javascript
 * Uses workboxBuild.injectManifest for SW configuration
 * Compiler configuration at the end of this file
 *
 * This compiler should be run AFTER all the assets are built
 */

// temp build file location
const TEMP_FILE_NAME = 'temp_sw.js'
const TEMP_FILE_PATH = __dirname + '/../build/' + TEMP_FILE_NAME

// 1 - compile from TypeScript to JavaScript
const compileTypescript = (conf) =>
  babel.transformFileAsync(conf.sourceFile, {
    presets: ['@babel/preset-typescript'],
  })

// 2 - save to temp file
const writeToTempFile = (data) =>
  new Promise((resolve, reject) => {
    fs.writeFile(TEMP_FILE_PATH, data, (err) => {
      if (err) {
        reject(err)
      }
      resolve(TEMP_FILE_PATH)
    })
  })

/**
 * 3 - build the Service Worker from the temp file
 * Credit how to modify CRA SW goes to:
 * https://medium.com/@chinmaya.cp/custom-service-worker-in-cra-create-react-app-3b401d24b875
 */
const buildSW = (sourceFile) =>
  workboxBuild
    .injectManifest({
      swSrc: sourceFile, // this is your sw template file
      swDest: 'build/sw.js', // this will be created in the build step
      globDirectory: 'build',
      globPatterns: [
        '**/*.{js,css,html,png,jpg}', // all matching files
        '**/site.webmanifest', // Web App Manifest
      ],
      globIgnores: [
        TEMP_FILE_NAME, // the temp sw file
        '**/service-worker.js', // the default CRA service worker
        '**/precache-manifest.*.js', // the default CRA precache manifest
        '**/pwa-assets/splash/*.png', // iOS splash screens
      ],
    })
    .then(({ count, size, warnings }) => {
      // Optionally, log any warnings and details.
      warnings.forEach(console.warn)
      console.log(`${count} files will be precached, totaling ${size} bytes.`)

      // remove the temp file
      fs.unlinkSync(TEMP_FILE_PATH)
    })

// Compiler config
const config = {
  sourceFile: './serviceWorker/index.ts',
}

// run the compiler
compileTypescript(config)
  .then((compiled) => writeToTempFile(compiled.code))
  .then((sourceFile) => buildSW(sourceFile))
  .then(() =>
    console.log('The service worker is compiled and ready in the /build folder')
  )
  .catch((error) => console.error(error))
