// The content of the service worker

// Declare workbox
/// <reference path='../node_modules/typescript/lib/lib.webworker.d.ts' />

// add _self instead of self int the correct typing
// eslint-disable-next-line no-restricted-globals
const _self = (self as unknown) as ServiceWorkerGlobalScope

if ('function' === typeof importScripts) {
  importScripts(
    'https://storage.googleapis.com/workbox-cdn/releases/3.5.0/workbox-sw.js'
  )

  // Global workbox
  if (workbox) {
    console.log('Workbox is loaded')

    // Enable logging
    workbox.setConfig({ debug: true })

    _self.addEventListener('install', (event) => {
      console.log('The service worker state is install.')
      _self.skipWaiting()
    })

    _self.addEventListener('activate', (event) => {
      console.log('The service worker state is activate.')
      event.waitUntil(_self.clients.claim())
    })

    // Manual injection point for manifest files.
    // All assets under build/ and 5MB sizes are precached.
    workbox.precaching.precacheAndRoute([])

    // return  app shell on all navigation requests
    workbox.routing.registerNavigationRoute('/index.html')

    // Google Fonts caching
    // the response is opaque !!
    workbox.routing.registerRoute(
      /^https:\/\/fonts\.(?:googleapis|gstatic)\.com/,
      new workbox.strategies.CacheFirst({
        cacheName: 'googleapis',
        plugins: [
          new workbox.cacheableResponse.Plugin({
            statuses: [0, 200],
          }),
          new workbox.expiration.Plugin({
            maxEntries: 30,
          }),
        ],
      })
    )

    // Restaurant Image caching
    // the response is opaque !!
    workbox.routing.registerRoute(
      /^https:\/\/www\.(?:googleapis)\.com\/download\/..*\/pingl..*restaurant-cover-photos/,
      new workbox.strategies.StaleWhileRevalidate({
        cacheName: 'restaurant-cover-photos',
        plugins: [
          new workbox.cacheableResponse.Plugin({
            statuses: [0, 200],
          }),
          new workbox.expiration.Plugin({
            maxEntries: 200,
            maxAgeSeconds: 30 * 24 * 60 * 60, // 30 Days
          }),
        ],
      })
    )

    // Image caching
    workbox.routing.registerRoute(
      /\.(?:png|gif|jpg|jpeg|svg)$/,
      new workbox.strategies.CacheFirst({
        cacheName: 'images',
        plugins: [
          new workbox.expiration.Plugin({
            maxEntries: 200,
            maxAgeSeconds: 30 * 24 * 60 * 60, // 30 Days
          }),
        ],
      })
    )

    // JS, CSS caching
    workbox.routing.registerRoute(
      /\.(?:js|css)$/,
      new workbox.strategies.StaleWhileRevalidate({
        cacheName: 'static-resources',
        plugins: [
          new workbox.expiration.Plugin({
            maxEntries: 60,
            maxAgeSeconds: 20 * 24 * 60 * 60, // 20 Days
          }),
        ],
      })
    )
  } else {
    console.error('Workbox could not be loaded. No offline support')
  }
}
